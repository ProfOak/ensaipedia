import { useState, useEffect } from "react";

import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";
import { Filesystem, Directory } from "@capacitor/filesystem";
import { Storage } from "@capacitor/storage";
import { Capacitor } from "@capacitor/core";
import store from "../js/store";
import { AndroidPermissions } from "@ionic-native/android-permissions";

export async function base64FromPath(path) {
  console.log(path);
  const response = await fetch(path, {
    method: "GET",
    mode: "cors",
    cache: "default",
  });
  const blob = await response.blob();
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      if (typeof reader.result === "string") {
        resolve(reader.result);
      } else {
        reject("method did not return a string");
      }
    };
    reader.readAsDataURL(blob);
  });
}

export async function savePicture(photo, fileName) {
  //   const base64Data = await base64FromPath(photo);
  console.log(photo);
  const response = await fetch(photo, {
    method: "GET",
    mode: "cors",
    cache: "default",
  });
  const blob = await response.blob();
  console.log(response);
  await Filesystem.writeFile({
    path: fileName,
    data: blob,
    directory: Directory.Data,
  });
  try {
    await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Documents,
    });
  } catch {
    console.log("Permission la famille error");
  }

  console.log("savePicture");
  console.log(Directory.ExternalStorage);
}

export async function usePhotoGallery(code_url) {
  if (Capacitor.isNativePlatform()) {
    AndroidPermissions.checkPermission(
      AndroidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
    ).then((result) => {
      if (result.hasPermission) {
        console.log("READ_EXTERNAL_STORAGE deja ok");
      } else {
        AndroidPermissions.requestPermission(
          AndroidPermissions.PERMISSION.READ_EXTERNAL_STORAGE
        );
      }
    });
  } else {
    console.log("Capacitor not detected, this button will do nothing :(");
  }

  if (Capacitor.isNativePlatform()) {
    AndroidPermissions.checkPermission(
      AndroidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
    ).then((result) => {
      if (result.hasPermission) {
        console.log("READ_EXTERNAL_STORAGE deja ok");
      } else {
        AndroidPermissions.requestPermission(
          AndroidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
        );
      }
    });
  } else {
    console.log("Capacitor not detected, this button will do nothing :(");
  }

  const path = code_url;
  const fileName = new Date().getSeconds() + ".jpg";
  await savePicture(path, fileName);
  console.log("Photo saved");
}
