import React, { useState, useEffect } from "react";
import { getDevice } from "../js/framework7-custom.js";
import {} from "../fonts/material-icons.css";
import {} from "../css/icons.css";
import connect from "../js/requests/api_aut";
import store from "../js/store";
import {
  f7,
  f7ready,
  App,
  Panel,
  Views,
  View,
  Popup,
  Page,
  Navbar,
  Toolbar,
  NavRight,
  Link,
  Block,
  BlockTitle,
  LoginScreen,
  LoginScreenTitle,
  List,
  ListItem,
  ListInput,
  ListButton,
  BlockFooter,
  useStore,
} from "framework7-react";

import capacitorApp from "../js/capacitor-app";
import routes from "../js/routes";
import reload_dex from "../js/requests/api_dex";

const MyApp = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const device = getDevice();

  const f7params = {
    name: "ENSAdex", // App name
    theme: "auto", // Automatic theme detection
    id: "ensadex", // App bundle ID
    store: store,
    routes: routes,
    input: {
      scrollIntoViewOnFocus: device.capacitor,
      scrollIntoViewCentered: device.capacitor,
    },
    statusbar: {
      iosOverlaysWebView: true,
      androidOverlaysWebView: false,
    },
  };
  async function sendconnect() {
    f7.dialog.preloader();
    await connect(username, password);
    if (store.state.token != "") {
      await reload_dex();
      f7.loginScreen.close();
      f7.dialog.close();
    } else {
      f7.dialog.close();
      f7.dialog.alert("Combinaison nom d'utilisateur/mot de passe incorrecte");
    }
  }

  f7ready(() => {
    // Init capacitor APIs (see capacitor-app.js)
    if (f7.device.capacitor) {
      capacitorApp.init(f7);
    }
    // Call F7 APIs here
  });

  return (
    <App {...f7params}>
      <Views tabs className="safe-areas">
        <Toolbar tabbar labels bottom colorTheme={"white"} themeDark={true}>
          <Link
            tabLink="#view-home"
            tabLinkActive
            iconIos="f7:house_fill"
            iconAurora="f7:house_fill"
            iconMd="material:home"
            text="Home"
          />
          <Link
            tabLink="#view-dex"
            iconIos="f7:square_list_fill"
            iconAurora="f7:square_list_fill"
            iconMd="material:view_list"
            text="ENSAdex"
          />
          <Link
            tabLink="#view-MyProfile"
            iconIos="f7:gear"
            iconAurora="f7:gear"
            iconMd="material:account_circle"
            text="Profil"
          />
        </Toolbar>

        <View id="view-home" main tab tabActive url="/" />

        {/* Dex View */}
        <View id="view-dex" name="dex" tab url="/dex/" />

        {/* Profile View */}
        <View id="view-MyProfile" name="profile" tab url="/MyProfile/" />
      </Views>
    </App>
  );
};

export default MyApp;
