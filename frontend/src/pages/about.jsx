import React, { useState } from "react";
import { useStore } from "framework7-react";
import {
  Page,
  Navbar,
  Block,
  Button,
  BlockTitle,
  NavTitle,
  Col,
  Row,
} from "framework7-react";
import store from "../js/store";
import check from "../js/requests/server_state";

const AboutPage = (props) => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const coltext2 = useStore("coltext2");
  const [server_state, setserver_state] = useState(store.state.health);
  const [indicator_color, setindicc] = useState(store.state.indicator_color);
  const [indicator_text, setindict] = useState(store.state.indicator_text);

  async function load() {
    console.log("oui");
    await check();
    setserver_state(store.state.health);
    setindicc(store.state.indicator_color);
    setindict(store.state.indicator_text);
  }

  return (
    <Page colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>A propos</NavTitle>
      </Navbar>
      <BlockTitle medium>Attrapez les tous !</BlockTitle>
      <Block strong>
        <p>
          L'objectif de l'ENSAdex est de capturer un maximum de qr-code des
          autres étudiants de l'école afin de faire le plus de rencontres ! Il
          suffit pour cela de t'armer du scanner et d'appuyer sur le bouton
          d'ajout lorsque tu es façe à un qr-code d'un autre ENSAIENS.
        </p>
        <p>
          Une fois un ENSAIENS recontré, tu peux consulter son profil dans
          l'ENSAdex. Tu apprendras alors si il appartient à une liste, son
          surnom et d'autres informations (in)utiles.
        </p>
        <p>
          Tu peux trouver des qr-code partout : dans l'école, sur le campus, par
          le biais des autres étudiants qui peuvent t'aider. Sois créatif : il
          existe un bouton qui permet d'exporter le qr-code dans l'onglet 'Mon
          code'. Cela permet de l'imprimer pour le déposer à l'endroit de ton
          choix. (Un frigo EJR defectueux, le bureau de Laurent Tardif, ou même
          le casier de ton/ta bien aimée !)
        </p>
      </Block>

      <BlockTitle textColor={coltext2}>Etat du serveur ENSADEX</BlockTitle>
      <Block strong>
        <Row>
          <Col width={33}>
            <Button fill color={indicator_color}>
              {indicator_text}
            </Button>
          </Col>
          <Col width={66}> {server_state} </Col>
        </Row>
      </Block>

      <BlockTitle medium>Remerciements </BlockTitle>
      <Block strong>
        <Row>
          <Col>
            <BlockTitle> Olivier Levitt </BlockTitle>
            <BlockTitle>Alexandre Bidon</BlockTitle>
          </Col>
          <Col>
            <BlockTitle>Hugo Miccinilli</BlockTitle>
          </Col>
        </Row>
      </Block>
    </Page>
  );
};

export default AboutPage;
