import React from "react";
import { useStore } from "framework7-react";
import { Page, Navbar, Block, NavTitle } from "framework7-react";

const NotFoundPage = () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");

  <Page colorTheme={theme} themeDark={isdark}>
    <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
      <NavTitle textColor={coltext1}>Not found</NavTitle>
    </Navbar>
    <Block strong>
      <p>Sorry</p>
      <p>Requested content not found.</p>
    </Block>
  </Page>;
};

export default NotFoundPage;
