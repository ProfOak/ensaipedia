import React, { useState } from "react";
import {
  Page,
  Navbar,
  Block,
  BlockTitle,
  NavTitle,
  Col,
  Row,
  Button,
  Icon,
} from "framework7-react";
import {
  exportComponentAsJPEG,
  exportComponentAsPDF,
  exportComponentAsPNG,
} from "react-component-export-image";
import store from "../js/store";
import { useStore } from "framework7-react";
import convertIdToList from "../js/converter";
import { usePhotoGallery } from "../hooks/usePhotoGallery";

import "../css/myCode.less";

const MycodePage = () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const url = store.state.url;
  const [url_img, seturlimage] = useState(
    url + "/qr_codes/" + store.state.user.qr_code + ".jpg"
  );

  const reload = (done) => {
    setTimeout(() => {
      seturlimage(url + "/qr_codes/" + store.state.user.qr_code + ".jpg");
      done();
    }, 1000);
  };

  return (
    <Page
      ptr
      ptrMousewheel={true}
      name="MyProfile"
      onPtrRefresh={reload}
      onPageTabShow={reload}
      colorTheme={theme}
      themeDark={isdark}
    >
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Partager son code</NavTitle>
      </Navbar>

      <Block strong>
        <img
          className="qr-code"
          alt="Votre QR_code charge, merci de patienter"
          src={url_img}
          width="300"
        />
      </Block>
      <p className="nom-personne">{store.state.user.name}</p>
      <p className="liste">{convertIdToList(store.state.user.group_id)}</p>
      <Row>
        <Col>
          <Button raised fill round onClick={() => usePhotoGallery(url_img)}>
            Exporter en JPG
          </Button>
        </Col>
      </Row>
    </Page>
  );
};

export default MycodePage;
