import React, { useState } from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Gauge,
  Segmented,
  Button,
  f7,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";
import "../css/home.less";
import check from "../js/requests/server_state";
import { Share } from "@capacitor/share";
import { Haptics, ImpactStyle } from "@capacitor/haptics";
import { Storage } from "@capacitor/storage";

const hapticsImpactMedium = async () => {
  await Haptics.impact({ style: ImpactStyle.Medium });
};

const HomePage = () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const coltext2 = useStore("coltext2");
  const [name, setname] = useState(store.state.user.name);
  const [server_state, setserver_state] = useState(store.state.health);
  const [indicator_color, setindicc] = useState(store.state.indicator_color);
  const [indicator_text, setindict] = useState(store.state.indicator_text);

  async function reload() {
    await check();
    setserver_state(store.state.health);
  }

  async function basicShare(length) {
    Share.share({
      title: "ENSADEX",
      text: "J'ai capturé " + length + " ENSAIENS dans mon ENSADEX !",
      url: "https://play.google.com/store/apps/details?id=io.ensadex&gl=FR",
      dialogTitle: "Partage",
    });
  }

  async function load() {
    await check();
    setserver_state(store.state.health);
    setindicc(store.state.indicator_color);
    setindict(store.state.indicator_text);
  }

  if (store.state.token == "") {
    return (
      <Page
        colorTheme={theme}
        themeDark={isdark}
        name="home"
        onPageTabShow={reload}
        onPageInit={load}
      >
        <Navbar colorTheme={coltext1} themeDark={true}>
          <NavTitle textColor={coltext1}>Bienvenue dans l'ENSAdex</NavTitle>
        </Navbar>
        <Block>
          <p>
            Bien le bonjour ! Bienvenue à l'ENSAI ! Mon nom est professeur
            Jougaud. Je fais parti des plus éminents professeurs de cet école.
            Ce monde est peuplé de créatures du nom d'ENSAIENS. L'étude des
            ENSAIENS est ma profession.
          </p>

          <p>
            Voici mon invention... L'ENSAdex ! Il enregistre les informations
            sur les ENSAIENS rencontrés. C'est comme une encyclopédie! Faire un
            guide complet sur les ENSAIENS de toute l'école ... C'est mon rêve!
            Mais je suis trop vieux maintenant. C'est pourquoi je veux que vous
            terminiez mon travail! Que la grande quête des ENSAIENS commence!
          </p>
        </Block>

        <Block>
          <Row>
            <Col>
              <Button
                fill
                raised
                href="/login/"
                round
                onClick={hapticsImpactMedium()}
              >
                Se connecter
              </Button>
            </Col>
          </Row>
          <p>
            Sans connexion, tu peux perdre les ENSAIENS rencontrés, et tu ne
            peux pas modifier ton profil.
          </p>
        </Block>

        <List>
          <ListItem link="/rankings/" title="Accéder aux classements" />
          <ListItem link="/about/" title="A propos de l'ENSAdex" />
          <ListItem link="/legal/" title="Mentions légales" />
        </List>

        <BlockTitle textColor={coltext2}>Etat du serveur ENSADEX</BlockTitle>
        <Block strong>
          <Row>
            <Col width={33}>
              <Button fill color={indicator_color} popoverOpen=".popover-menu">
                {indicator_text}
              </Button>
            </Col>
            <Col width={66}> {server_state} </Col>
          </Row>
        </Block>
      </Page>
    );
  } else {
    return (
      <Page
        name="home"
        colorTheme={theme}
        themeDark={isdark}
        onPageTabShow={reload}
      >
        <Navbar colorTheme="white" themeDark={true}>
          <NavTitle textColor={coltext1}>
            {"Bienvenue " + store.state.user.name + " !"}
          </NavTitle>
        </Navbar>
        <Block>
          <Col className="text-align-center">
            <Gauge
              type="semicircle"
              value={(1 + store.state.profiles.length) / 250}
              valueText={1 + store.state.profiles.length}
              valueTextColor={coltext2}
              borderColor={coltext1}
              labelText="sur 250"
              labelTextColor={coltext2}
            />
          </Col>
        </Block>
        <Block>
          <p>
            Tu as rencontré {store.state.profiles.length + 1} ensaiens depuis le
            début de l'aventure ! J'espère que tu en trouveras de nouveaux
            aujourd'hui.
          </p>

          <p>
            N'hésites pas à consulter le profil des ensaiens rencontrés dans ton
            ENSAdex !
          </p>
        </Block>

        <Block>
          <Row>
            <Col width={50}>
              <Button
                href="/mycode/"
                slot="right"
                iconIos="f7:qr"
                iconAurora="f7:qr"
                iconMd="material:qr_code"
                text="Partage ton code"
                raised
                fill
                large
              ></Button>
            </Col>
            <Col width={50}>
              <Button
                slot="right"
                href="/scanner/"
                iconIos="f7:camera"
                iconAurora="f7:camera"
                iconMd="material:center_focus_weak"
                text="Ajoute un ENSAIEN"
                raised
                fill
                large
              ></Button>
            </Col>
          </Row>
        </Block>

        <List>
          <ListItem link="/rankings/" title="Accéder aux classements" />
          <ListItem link="/about/" title="A propos de l'ENSAdex" />
        </List>
        <Row>
          <Col>
            <Button
              fill
              raised
              onClick={() => basicShare(store.state.profiles.length)}
              round
            >
              Partage ton score !
            </Button>
          </Col>
        </Row>
      </Page>
    );
  }
};

export default HomePage;
