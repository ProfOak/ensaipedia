import React, { useState } from "react";
import {
  Page,
  Navbar,
  List,
  Link,
  ListInput,
  ListItem,
  Toggle,
  NavTitle,
  BlockTitle,
  Row,
  Col,
  Button,
  Range,
  Block,
  Icon,
} from "framework7-react";
import store from "../js/store";
import "../css/myprofile.less";
import { useStore } from "framework7-react";
import convertIdToList from "../js/converter";

const MyProfilePage = (props) => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const [myprofileid, setmyprofileid] = useState(store.state.user.id);
  const [myprofilename, setmyprofilename] = useState(store.state.user.name);
  const [myprofilesurname, setmyprofilesurname] = useState(
    store.state.user.surname
  );
  const [myprofileliste, setmyprofileliste] = useState(
    store.state.user.group_id
  );
  const [myprofiledes, setmyprofiledes] = useState(
    store.state.user.description
  );

  const url = store.state.url;
  const url_img = url + "/profile_pictures/" + store.state.user.picture;

  function load() {
    setmyprofilename(store.state.user.name);
    setmyprofilesurname(store.state.user.surname);
    setmyprofileliste(store.state.user.group_id);
    setmyprofiledes(store.state.user.description);
  }

  function reload(done) {
    setTimeout(() => {
      setmyprofilename(store.state.user.name);
      setmyprofilesurname(store.state.user.surname);
      setmyprofileliste(store.state.user.group_id);
      setmyprofiledes(store.state.user.description);
      done();
    }, 1000);
  }

  return (
    <Page
      ptr
      ptrMousewheel={true}
      name="MyProfile"
      onPtrRefresh={reload}
      onPageTabShow={load}
      ontabShow={load}
      colorTheme={theme}
      themeDark={isdark}
    >
      <Navbar colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Mon profil</NavTitle>
      </Navbar>
      <Block>
        <img className="picture" src={url_img}></img>
        <p className="name">{store.state.user.name}</p>
        <p className="surname">Surnom : {store.state.user.surname}</p>
        <p className="liste-perso">
          Liste : {convertIdToList(store.state.user.group_id)}
        </p>
        <p className="bio">{store.state.user.description}</p>

        <Button fill raised href="/mycode/" round>
          Obtenir mon qr-code
        </Button>

        <List>
          <ListItem link="/profileeditor/" title="Modifier mon profil" />
          <ListItem link="/options/" title="Options" />
          <ListItem link="/legal/" title="Mentions légales" />
          <ListItem link="/contact/" title="Contacter les auteurs" />
        </List>
      </Block>
    </Page>
  );
};

export default MyProfilePage;
