import React from "react";
import {
  Page,
  Navbar,
  BlockTitle,
  Block,
  useStore,
  Col,
  NavTitle,
  Row,
} from "framework7-react";
import store from "../js/store";
import "../css/profile.less";
import convertIdToList from "../js/converter";

const ProfilePage = (props) => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const profileId = props.f7route.params.id;
  const profiles = useStore("profiles");
  const example = store.state.example;

  var currentprofile;
  var currentShiney;

  profiles.forEach(function (profile) {
    if (profile.id === profileId) {
      currentprofile = profile;
      currentShiney = "Aucune version shiney découverte.";
      if (currentprofile.shiney) {
        currentShiney = "Existe en version shiney.";
      }
    }
  });
  if (profileId == "0") {
    currentprofile = example;
    currentShiney = "Aucune version shiney découverte.";
  }

  const url = store.state.url;
  const url_img = url + "/profile_pictures/" + currentprofile.picture;

  return (
    <Page name="profile" colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>{currentprofile.name}</NavTitle>
      </Navbar>

      <Block></Block>
      <img className="photo_pr" src={url_img}></img>
      <Block>
        <Row>
          <Col>
            <p className="liste_current_pr">
              Liste : {convertIdToList(currentprofile.group_id)}
            </p>
          </Col>
          <Col>
            <p className="surname_pr">Surnom : {currentprofile.surname}</p>
          </Col>
        </Row>
      </Block>

      <p className="description_pr"> {currentprofile.description}</p>
      <p className="shiney"> {currentShiney}</p>
    </Page>
  );
};

export default ProfilePage;
