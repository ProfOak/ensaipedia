import React from "react";
import {
  Page,
  Navbar,
  Block,
  BlockTitle,
  Button,
  NavTitle,
  List,
  ListItem,
  Row,
  Col,
} from "framework7-react";
import { useState } from "react";
import { QrReader } from "react-qr-reader";
import "../css/scanner.less";
import { width } from "dom7";
import store from "../js/store";
import { useStore } from "framework7-react";
import profile_add from "../js/requests/api_scan_analysis";

const ScannerPage = (props) => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const [data, setData] = useState("No result");

  const [DispData, setDispData] = useState("Pas de résultat");

  function resultmaker(data) {
    if (data != "No Result") {
      setDispData("ENSAIEN detecté !");
    }
  }

  const DisplayENSAIEN = (response) => {
    /* Sortir un popup */
  };

  return (
    <Page colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Ajouter des ENSAIENS</NavTitle>
      </Navbar>

      <>
        <div className="videostyle">
          <QrReader
            className="QrReader"
            constraints={{ facingMode: "environment" }}
            onResult={(result, error) => {
              if (!!result) {
                setData(result?.text);
                resultmaker(data);
              }
              if (!!error) {
                console.info(error);
              }
            }}
          />
        </div>
        <Block strong>
          <Row>
            <Col>
              <Button
                fill
                raised
                onClick={() => {
                  if (data != "No result") {
                    profile_add(data);
                  }
                }}
                round
              >
                Ajouter l'ENSAIEN à ma base
              </Button>
            </Col>
          </Row>
          <p className="result">{DispData}</p>
        </Block>
      </>
    </Page>
  );
};

export default ScannerPage;
