import React from "react";
import {
  Page,
  Navbar,
  Block,
  BlockTitle,
  useStore,
  NavTitle,
} from "framework7-react";
import store from "../js/store";
import "../css/legal-mentions.less";

const LegalPage = () => {
  const theme = useStore("color");
  const isdark = useStore("dark");
  const coltext1 = useStore("coltext1");
  return (
    <Page colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Mentions légales</NavTitle>
      </Navbar>
      <Block strong>
        <p>
          Les présentes Conditions Générales régissent l’utilisation du site web
          « inserer url ici» (ci-après le «Site») et des applications mobiles
          ENSAdex en versions Android® et iOS® (ci-après les «Applications»).
          Conformément à la loi, nous vous invitons à prendre connaissance des
          présentes conditions d’utilisation (ci-après les « Conditions »).
        </p>
        <p>
          Ces Conditions portent sur vos droits et responsabilités légales
          applicables dès lors que vous utilisez ce Site. Le Site et les
          Applications sont mis à votre disposition pour votre usage personnel,
          sous réserve du respect des Conditions définies ci-après. En accédant
          à ce Site et aux Applications, en les visitant et/ou en les utilisant,
          vous reconnaissez que vous avez lu, que vous avez compris et que vous
          acceptez d’être lié par ces Conditions, de même que vous vous engagez
          à vous conformer à l’ensemble des lois et règlements applicables. Si
          vous n’acceptez pas ces Conditions, veuillez ne pas utiliser le
          présent Site et les Applications.
        </p>
        <p>
          Vous vous engagez à accéder aux informations figurant sur ce Site et
          sur les Applications uniquement pour un usage légal. Vous ne pouvez
          utiliser le Site et les Applications que dans un but légitime ; aucune
          utilisation, ou détournement n’est autorisé. Le contenu de la présente
          notice peut être amené à changer ; nous vous invitons par conséquent à
          la consulter très régulièrement en vérifiant la date de cette dernière
        </p>
      </Block>
      <BlockTitle>Données personelles</BlockTitle>
      <Block strong>
        <p>
          Cette application collecte des données personelles dans le cadre légal
          prévu par le RPGD. Nous prenons l'engagement de ne pas céder ou
          partager ces données personelles à une tierce personne. Nous
          n'utiliserons pas ces données à des fins commerciales ou de recherche.
          Elles sont uniquement utilisées dans le cadre du fonctionnement de
          cette application et seront automatiquement supprimées en cas d'arrêt
          du fonctionnement.
        </p>
        <p>
          Vous pouvez accéder à vos données personelles en prenant contact avec
          les auteurs de cette application et les faire supprimer. Vous avez
          également la possibilité de supprimer vous même votre compte dans
          l'onglet profil. Cette action entraine automatiquement la suppression
          de vos données des bases de données. Par conséquent, vous n'aurez pas
          la possibilité de récupérer votre compte en cas de supression.
          Cependant, il vous est possible de demander la création d'un nouveau
          compte personnel en prenant contact avec les auteur. En utilisant
          cette application, vous acceptez tacitement la collecte de ces données
          et ne pouvez vous y opposer car elles sont nécéssaires à son
          fonctionnement. Si ce n'étais pas le cas, nous vous invitons à ne pas
          utiliser l'application et fermer votre compte. Nous vous présentons
          nos excuses par avance pour la gène occasionée.
        </p>
      </Block>
      <BlockTitle>Propriété intelectuelle</BlockTitle>
      <Block strong>
        <p>
          Cette application est publiée sous licence GPU GPL 3, son code est
          intégralement open-source et toute personne souhaitant réutiliser ce
          code le peut à la condition de respecter les conditions posées par la
          licence disponible sur le projet gitlab.com.
        </p>
        <p>
          Si c'était le cas, nous vous invitons à prendre contact avec nous. Les
          coordonées des auteurs sont disponibles dans la section 'contacter les
          auteurs', dans l'onglet profil. Vous pouvez également utiliser le
          formulaire de contact. Nous serons ravis de vous discuter avec vous,
          que cela soit pour mettre en place cette application dans un nouveau
          cadre ou l'améliorer
        </p>
      </Block>

      <p className="authors"> Titouan Rigaud - Elwenn Joubrel</p>
    </Page>
  );
};

export default LegalPage;
