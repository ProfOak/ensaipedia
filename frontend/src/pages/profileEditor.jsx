import React, { useState } from "react";
import "../css/profile_editor.less";
import { width } from "dom7";
import store from "../js/store";
import { f7, useStore } from "framework7-react";
import modify_profile from "../js/requests/api_modify_profile";
import {
  Page,
  Navbar,
  List,
  NavTitle,
  ListInput,
  ListItem,
  Icon,
  Toggle,
  BlockTitle,
  Row,
  Col,
  Button,
  Range,
  Block,
} from "framework7-react";
import { Camera, CameraResultType, CameraSource } from "@capacitor/camera";

export default ({ f7router }) => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const url = store.state.url;
  const [url_img, setUrlImg] = useState(
    url + "/profile_pictures/" + store.state.user.picture
  );

  async function SendForm(data) {
    data.preventDefault();
    f7.dialog.confirm(
      "Etes vous sur de vouloir apporter ces modifications ?",
      async () => {
        f7.dialog.preloader("Modification du profil en cours");
        await modify_profile(
          store.state.user.name,
          data.target[0].value,
          data.target[2].value,
          data.target[1].value
        );
        f7.dialog.close();
        f7router.back();
      }
    );
  }

  async function getPicture() {
    const image = await Camera.getPhoto({
      source: "PHOTOS",
      resultType: CameraResultType.DataUrl,
    });
    setUrlImg(image.dataUrl);
  }

  return (
    <Page name="Profileeditor" colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Edition du profil</NavTitle>
      </Navbar>
      <Block>
        <img className="picture-edit" src={url_img}></img>
      </Block>
      <Block>
        <Row>
          <Col></Col>
          <Col>
            <Button
              outline
              large
              className="icon-add"
              onClick={() => getPicture()}
            >
              <Icon
                ios="f7:add_photo_alternate"
                aurora="f7:add_photo_alternate"
                md="material:add_photo_alternate"
                color={coltext1}
                size={40}
              ></Icon>
            </Button>
          </Col>
          <Col></Col>
        </Row>
      </Block>
      <List form onSubmit={(data) => SendForm(data)} noHairlinesMd>
        <ListInput
          label="Surnom"
          type="text"
          placeholder={store.state.user.surname}
          defaultValue={store.state.user.surname}
        ></ListInput>

        <ListInput
          label="Type"
          type="select"
          defaultValue={store.state.user.group_id}
          placeholder="Normal"
        >
          <option value={0}>Normal</option>
          <option value={4}>Hakuna Madata</option>
          <option value={6}>MugiData</option>
          <option value={5}>Grands Dataquants</option>
          <option value={3}>Empire Contrastat</option>
          <option value={1}>Sexion Datassaut</option>
          <option value={2}>Prostats</option>
        </ListInput>

        <ListInput
          type="textarea"
          label="Description"
          placeholder={store.state.user.description}
          defaultValue={store.state.user.description}
        ></ListInput>

        <p className="warning">
          Attention : avec un compte invité, la modification n'aura pas d'effet
        </p>
        <Button type="submit" className="col" round fill>
          Sauvegarder les modifications
        </Button>
      </List>
    </Page>
  );
};
