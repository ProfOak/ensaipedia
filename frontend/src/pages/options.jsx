import React from "react";
import "../css/profile_editor.less";
import { width } from "dom7";
import store from "../js/store";
import { useStore } from "framework7-react";
import modify_profile from "../js/requests/api_modify_profile";
import {
  Page,
  Navbar,
  Radio,
  BlockHeader,
  NavTitle,
  ListItem,
  List,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Range,
  Block,
} from "framework7-react";

export default () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");

  const SelectTheme = (data) => {
    const value = data.target.value;
    if (value == "ENSAI") {
      store.state.color = "lightblue";
      store.state.dark = false;
      store.state.coltext1 = "lightblue";
      store.state.coltext2 = "gray";
    }
    if (value == "Dark") {
      store.state.color = "lightblue";
      store.state.dark = true;
      store.state.coltext1 = "lightblue";
      store.state.coltext2 = "gray";
    }
    if (value == "HUB") {
      store.state.color = "deeporange";
      store.state.dark = true;
      store.state.coltext1 = "deeporange";
      store.state.coltext2 = "orange";
    }
    if (value == "Hakuna") {
      store.state.color = "yellow";
      store.state.dark = false;
      store.state.coltext1 = "yellow";
      store.state.coltext2 = "gray";
    }
  };

  return (
    <Page name="options" colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Options</NavTitle>
      </Navbar>

      <BlockTitle>Thème de l'application</BlockTitle>
      <List mediaList>
        <ListItem
          radio
          radioIcon="end"
          title="ENSAI - clair"
          subtitle="Un thème simple, aux couleurs de votre école"
          value="ENSAI"
          name="theme"
          onChange={(data) => SelectTheme(data)}
        ></ListItem>
        <ListItem
          radio
          radioIcon="end"
          title="ENSAI - sombre"
          subtitle="Pour ceux qui passent plus de temps en chartres qu'à l'école"
          value="Dark"
          onChange={(data) => SelectTheme(data)}
          name="theme"
        ></ListItem>
        <ListItem
          radio
          radioIcon="end"
          title="ENSAHUB"
          subtitle="Se passe de commentaire (dédicace à Louis Perraud)"
          value="HUB"
          onChange={(data) => SelectTheme(data)}
          name="theme"
        ></ListItem>
        <ListItem
          radio
          radioIcon="end"
          title="BDE"
          value="Hakuna"
          subtitle="Faut bien financer l'application ..."
          onChange={(data) => SelectTheme(data)}
          name="theme"
        ></ListItem>
      </List>
    </Page>
  );
};
