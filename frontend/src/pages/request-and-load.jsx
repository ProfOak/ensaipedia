import React from "react";
import { Page, Navbar, Block, List, ListItem ,NavTitle,} from "framework7-react";
import { useStore } from "framework7-react";

const RequestAndLoad = (props) => {
  const { user } = props;
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");

  return (
    <Page colorTheme={theme} themeDark={isdark}>
      <Navbar
        title={`${user.firstName} ${user.lastName}`}
        backLink="Back"
        colorTheme="white"
        themeDark={true}
      />
      <Block strong>{user.about}</Block>
      <List>
        {user.links.map((link, index) => (
          <ListItem
            key={index}
            link={link.url}
            title={link.title}
            external
            target="_blank"
          ></ListItem>
        ))}
      </List>
    </Page>
  );
};

export default RequestAndLoad;
