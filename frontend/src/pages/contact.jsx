import React from "react";
import store from "/js/store.js";
import { useState } from "react";
import "../css/contact.less";
import apiKey from "/js/emailkey.js";
import { useRef } from "react";
import emailjs from "@emailjs/browser";

import {
  Page,
  Navbar,
  Block,
  Button,
  BlockTitle,
  List,
  ListItem,
  ListInput,
  NavTitle,
} from "framework7-react";
import { useStore } from "framework7-react";
import { touchend } from "dom7";

export default () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const [request, setrequest] = useState("");
  function sendForm(data) {
    data.preventDefault();

    emailjs
      .sendForm(
        apiKey.SERVICE,
        apiKey.TEMPLATE_ID,
        data.target,
        apiKey.PUBLIC_KEY
      )

      .then(
        () => {
          setrequest(
            "Etat de votre demande : Votre remarque a été envoyée, merci de votre retour. Nous nous efforçons d'y répondre rapidement"
          );
        },
        (e) => {
          setrequest(
            "Etat de votre demande : Votre demande a échouée, merci de la réitérer plus tard"
          );
        }
      );
  }

  return (
    <Page colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Contact</NavTitle>
      </Navbar>

      <BlockTitle>Formulaire de contact</BlockTitle>
      <List form onSubmit={(data) => sendForm(data)} noHairlinesMd>
        <ListInput
          label="Objet"
          type="text"
          name="objet"
          placeholder="Objet de la demande"
          required
        ></ListInput>

        <ListInput
          type="textarea"
          label="Description"
          name="Description"
          placeholder="Decrivez ici votre suggestion. Si la demande concerne votre compte, précisez votre nom"
          required
        ></ListInput>

        <Button type="submit" className="col" round fill>
          Envoyer la demande
        </Button>

        <p className="demand_state">{request}</p>
      </List>
      <Block strong>
        <p>
          Nous avons développé ce projet dans le but de rassembler un maximum de
          personnes ensembles à l'école. Il continuera à vivre, même après notre
          départ. Nous vous invitons à contribuer au projet dans le cadre du
          gitlab mis en place. L'ensemble du code source est public et sous
          licence open-source.
        </p>

        <p> https://gitlab.com/ProfOak/ensaipedia</p>

        <p>
          Si vous rencontrez un problème, faites nous en part via le formulaire
          de contact, ou par les adresses de contact ci-dessous
        </p>
      </Block>

      <BlockTitle>Elwenn Joubrel</BlockTitle>
      <List simpleList>
        <ListItem title="Gitlab/Github : @ejoubrel"></ListItem>
        <ListItem title="Mail : e.joubrel@orange.fr"></ListItem>
        <ListItem title="Messenger : @elwennjoubrel"></ListItem>
      </List>
      <BlockTitle>Titouan Rigaud</BlockTitle>
      <List simpleList>
        <ListItem title="Gitlab/Github : @ProfOak"></ListItem>
        <ListItem title="Mail : titouanrigaud@gmail.com"></ListItem>
        <ListItem title="Messenger : @titouanrigaud"></ListItem>
      </List>
    </Page>
  );
};
