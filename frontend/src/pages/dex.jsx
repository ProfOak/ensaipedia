import React, { useState } from "react";
import {
  Page,
  Navbar,
  Subnavbar,
  NavTitle,
  List,
  ListItem,
  ListGroup,
  Block,
  Link,
  Button,
  useStore,
  theme,
  Searchbar,
  BlockTitle,
} from "framework7-react";
import store from "../js/store";
import { element } from "prop-types";
import reload_dex from "../js/requests/api_dex";
import "../css/dex.less";

/* 
Normal -> 0
Sexion Datassaut -> 1
Prostats -> 2
Empire Contrastat -> 3
Hakuna Madata -> 4
Grands Dataquants -> 5
MugiData -> 6
*/

const dexPage = (props) => {
  const url = store.state.url;
  const isdark = useStore("dark");
  const color = useStore("color");
  const coltext1 = useStore("coltext1");
  const coltext2 = useStore("coltext2");
  function appartient_liste(profile, LISTE, repart) {
    if (profile.group_id == repart) {
      LISTE.push(profile);
    }
  }

  function Arrayliste(nom_liste, profiles) {
    let origin = [];
    profiles.forEach(function (item, index) {
      appartient_liste(item, origin, nom_liste);
    });
    return origin;
  }

  var profiles = useStore("profiles");
  const [profilesNE, setprofilesNE] = useState(Arrayliste(0, profiles));
  const [profilesSD, setprofilesSD] = useState(Arrayliste(1, profiles));
  const [profilesPS, setprofilesPS] = useState(Arrayliste(2, profiles));
  const [profilesEC, setprofilesEC] = useState(Arrayliste(3, profiles));
  const [profilesHM, setprofilesHM] = useState(Arrayliste(4, profiles));
  const [profilesGD, setprofilesGD] = useState(Arrayliste(5, profiles));
  const [profilesMD, setprofilesMD] = useState(Arrayliste(6, profiles));
  const example = store.state.example;

  async function reload(done) {
    await reload_dex();
    setprofilesNE(Arrayliste(0, profiles));
    setprofilesSD(Arrayliste(1, profiles));
    setprofilesPS(Arrayliste(2, profiles));
    setprofilesEC(Arrayliste(3, profiles));
    setprofilesHM(Arrayliste(4, profiles));
    setprofilesGD(Arrayliste(5, profiles));
    setprofilesMD(Arrayliste(6, profiles));
    done();
  }

  async function load(done) {
    setprofilesNE(Arrayliste(0, profiles));
    setprofilesSD(Arrayliste(1, profiles));
    setprofilesPS(Arrayliste(2, profiles));
    setprofilesEC(Arrayliste(3, profiles));
    setprofilesHM(Arrayliste(4, profiles));
    setprofilesGD(Arrayliste(5, profiles));
    setprofilesMD(Arrayliste(6, profiles));
    done();
  }

  return (
    <Page
      name="dex"
      ptr
      ptrMousewheel={true}
      onPageTabShow={load}
      onPtrRefresh={reload}
      colorTheme={color}
      themeDark={isdark}
    >
      <Navbar colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>ENSAIENS rencontrés</NavTitle>
        <Link
          slot="right"
          href="/mycode/"
          iconIos="f7:qr"
          iconAurora="f7:qr"
          iconMd="material:qr_code"
          iconColor={coltext1}
        ></Link>
        <Link
          slot="right"
          href="/scanner/"
          iconIos="f7:camera"
          iconAurora="f7:camera"
          iconMd="material:center_focus_weak"
          iconColor={coltext1}
        ></Link>
        <Subnavbar inner={false}>
          <Searchbar
            searchContainer=".search-list"
            searchIn=".item-title"
            disableButton={!theme.aurora}
            placeholder="Rechercher un ENSAIEN"
          ></Searchbar>
        </Subnavbar>
      </Navbar>

      <Block>
        <BlockTitle textColor={coltext2}>Sexion Datassaut</BlockTitle>
        <List mediaLists className="search-list">
          {profilesSD.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Prostats</BlockTitle>
        <List mediaLists className="search-list">
          {profilesPS.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Empire Contrastat</BlockTitle>
        <List mediaLists className="search-list">
          {profilesEC.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Hakuna Madata</BlockTitle>
        <List mediaLists className="search-list">
          {profilesHM.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Grands Dataquants</BlockTitle>
        <List mediaLists className="search-list">
          {profilesGD.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Mugidata</BlockTitle>
        <List mediaLists className="search-list">
          {profilesMD.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
        </List>
      </Block>

      <Block>
        <BlockTitle textColor={coltext2}>Normal</BlockTitle>
        <List mediaLists className="search-list">
          {profilesNE.map((profile) => (
            <ListItem
              className="imagelist"
              media={url + "/profile_pictures/" + profile.picture}
              key={profile.id}
              title={profile.name}
              link={`/profile/${profile.id}/`}
            />
          ))}
          <ListItem
            title={example.name}
            link={`/profile/${0}/`}
            className="imagelist"
            media={url + "/profile_pictures/" + example.picture}
          />
        </List>
      </Block>
    </Page>
  );
};

export default dexPage;
