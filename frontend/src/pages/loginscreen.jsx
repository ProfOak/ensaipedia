import React, { useState } from "react";
import {
  f7,
  Page,
  LoginScreenTitle,
  List,
  ListInput,
  ListButton,
  BlockFooter,
  useStore,
} from "framework7-react";
import connect from "../js/requests/api_aut";
import store from "../js/store";
import reload_dex from "../js/requests/api_dex";

export default ({ f7router }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");

  async function sendconnect() {
    f7.dialog.preloader("Connexion en cours");
    await connect(username, password);
    if (store.state.token != "") {
      await reload_dex();
      f7.dialog.close();
      f7router.back();
    } else {
      f7.dialog.close();
      f7.dialog.alert("Combinaison nom d'utilisateur/mot de passe incorrecte");
    }
  }
  return (
    <Page
      noToolbar
      noNavbar
      noSwipeback
      loginScreen
      colorTheme={theme}
      themeDark={isdark}
    >
      <LoginScreenTitle>Se connecter</LoginScreenTitle>
      <List form>
        <ListInput
          type="text"
          name="username"
          placeholder="Nom d'utilisateur"
          value={username}
          onInput={(username) => setUsername(username.target.value)}
        ></ListInput>
        <ListInput
          type="password"
          name="password"
          placeholder="Mot de passe"
          value={password}
          onInput={(password) => setPassword(password.target.value)}
        ></ListInput>
      </List>
      <List>
        <ListButton
          title="Connexion"
          onClick={() => sendconnect()}
        ></ListButton>
        <BlockFooter>
          Ne donnez pas votre mot de passe à un autre utilisateur.
          <br />
          Utilisez des mots de passe uniques sur chaque site.
        </BlockFooter>
      </List>
    </Page>
  );
};
