import React from "react";
import {
  Page,
  Navbar,
  BlockTitle,
  List,
  ListItem,
  Icon,
  ListGroup,
  Toggle,
  NavTitle,
  BlockFooter,
  Block,
} from "framework7-react";
import store from "../js/store";
import { useStore } from "framework7-react";

const rankingsPage = () => {
  const isdark = useStore("dark");
  const theme = useStore("color");
  const coltext1 = useStore("coltext1");
  const rankingHunt = [{ id: 1, name: "professeur Chen", nb_capt: 250 }];
  const rankingAppear = [{ id: 1, name: "professeur Chen", nb_capt: 250 }];

  return (
    <Page name="rankings" colorTheme={theme} themeDark={isdark}>
      <Navbar backLink="Back" colorTheme={coltext1} themeDark={true}>
        <NavTitle textColor={coltext1}>Classements</NavTitle>
      </Navbar>

      <p className="title">Classement captures</p>

      <Block>
        <p>
          {" "}
          Cette page est temporaire, elle sera bientôt remplacée par le vrai
          classement{" "}
        </p>
      </Block>
      <List>
        {rankingHunt.map((profile) => (
          <ListItem
            key={profile.id}
            title={profile.name}
            after={profile.nb_capt}
          />
        ))}
      </List>
      <p className="title">Classement présence</p>
      <List>
        {rankingAppear.map((profile) => (
          <ListItem
            key={profile.id}
            title={profile.name}
            after={profile.nb_capt}
          />
        ))}
      </List>
    </Page>
  );
};

export default rankingsPage;
