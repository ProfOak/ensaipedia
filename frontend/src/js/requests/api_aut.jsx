import React from "react";
import store from "../store";
import actualise_user from "./api_actualise_user";

async function connect(username, password) {
  const url = store.state.url;
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
  myHeaders.append("Accept", "application/json");

  var myBody =
    "grant_type=&username=" +
    username +
    "&password=" +
    password +
    "&scope=&client_id=&client_secret=";

  try {
    const response = await fetch(url + "/token", {
      body: myBody,
      headers: myHeaders,
      method: "POST",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();
    store.state.token = result.access_token;
    await actualise_user();
  } catch (err) {
    console.log(err);
  }
}

export default connect;
