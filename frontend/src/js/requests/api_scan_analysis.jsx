import React from "react";
import store from "../store";
import { Haptics, ImpactStyle } from "@capacitor/haptics";

const hapticsImpactMedium = async () => {
  await Haptics.impact({ style: ImpactStyle.Medium });
};

async function profile_add(profile_id) {
  hapticsImpactMedium();
  const url = store.state.url;
  const token = store.state.token;

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + token);
  myHeaders.append("accept", "application/json");

  try {
    const response = fetch(url + "/me/capture?profile_id=" + profile_id, {
      headers: myHeaders,
      method: "PATCH",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();
  } catch (err) {
    console.log(err);
  }
}

export default profile_add;
