import React from "react";
import store from "../store";

async function reload_dex() {
  const url = store.state.url;
  const token = store.state.token;

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + token);
  myHeaders.append("accept", "application/json");

  try {
    const response = await fetch(url + "/me/user/with_dex", {
      headers: myHeaders,
      method: "GET",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();

    store.state.profiles = result.dex;
  } catch (err) {
    console.log(err);
  }
}

export default reload_dex;
