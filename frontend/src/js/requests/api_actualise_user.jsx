import React from "react";
import store from "../store";

async function actualise_user() {
  const url = store.state.url;
  const token = store.state.token;

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + token);
  myHeaders.append("accept", "application/json");

  try {
    const response = await fetch(url + "/me/user/with_profile", {
      headers: myHeaders,
      method: "GET",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();
    store.state.user.profile_id = result.profile_id;
    store.state.user.id = result.id;
    console.log(result);
    const profile = result.profile;
    console.log(profile);
    if (result.profile !== null) {
      store.state.user.name = profile.name;
      store.state.user.surname = profile.surname;
      store.state.user.picture = profile.picture;
      store.state.user.description = profile.description;
      store.state.user.group_id = profile.group_id;
      store.state.user.qr_code = profile.qr_code;
    }
  } catch (err) {
    console.log(err);
  }
}

export default actualise_user;
