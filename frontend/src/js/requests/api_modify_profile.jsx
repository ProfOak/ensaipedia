import React from "react";
import store from "../store";

async function modify_profile(name, surname, description, group_id) {
  const url = store.state.url;
  const token = store.state.token;

  console.log(name, surname, description, group_id);
  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + token);
  myHeaders.append("accept", "application/json");
  myHeaders.append("Content-Type", "application/json");

  const body = {
    name: name,
    surname: surname,
    description: description,
    group_id: group_id,
  };

  try {
    const response = await fetch(url + "/me/profile/update", {
      body: JSON.stringify(body),
      headers: myHeaders,
      method: "PATCH",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();
    console.log(result);

    store.state.user.name = result.name;
    store.state.user.surname = result.surname;
    store.state.user.description = result.description;
    store.state.user.group_id = result.group_id;
  } catch (err) {
    console.log(err);
  }
}

export default modify_profile;
