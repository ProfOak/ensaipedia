import React from "react";
import store from "../store";
async function profile_data(profileid) {
  const url = store.state.url;
  const token = store.state.token;

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + token);
  myHeaders.append("accept", "application/json");

  try {
    const response = fetch(url + "/profiles/" + profileid, {
      headers: myHeaders,
      method: "GET",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();

    console.log("faire un truc ici");
  } catch (err) {
    console.log(err);
  }
}

export default profile_data;
