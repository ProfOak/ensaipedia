import React from "react";
import store from "../store";

async function check() {
  const url = store.state.url;

  var myHeaders = new Headers();
  myHeaders.append("Accept", "application/json");

  try {
    const response = await fetch(url + "/health", {
      headers: myHeaders,
      method: "GET",
      mode: "cors",
      cache: "default",
    });

    console.log("status code: ", response.status);

    if (!response.ok) {
      console.log(response);
      throw new Error(`Error! status: ${response.status}`);
    }
    store.state.health = "Le serveur est prêt. Bonnes captures à tous ! ";
    store.state.indicator_color = "green";
    store.state.indicator_text = "Ok !";
  } catch (err) {
    store.state.health =
      "Nous n'avons pas réussi à joindre le serveur. Vérifiez votre connexion internet et réessayez plus tard";
    store.state.indicator_color = "red";
    store.state.indicator_text = "Erreur";
    console.log(err);
  }
}

export default check;
