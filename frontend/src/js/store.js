import { createStore } from "framework7/lite";

const store = createStore({
  state: {
    dark: false,
    color: "lightblue",
    coltext1: "lightblue",
    coltext2: "black",
    health: "Nous contactons le serveur ENSAI, merci de patienter",
    indicator_color: "blue",
    indicator_text: "Chargement",
    example: {
      name: "Professeur Chen",
      surname: "Prof. Oak",
      description:
        "Vous pourrez voir la liste des ensaiens capturés dans l'ENSADEX. Bon courage ! Je suis sûr que vous pouvez tous les attraper !",
      picture: "default_profile.png",
      group_id: 0,
      id: "0",
    },
    profiles: [],
    ind: 0,
    token: "",
    user: {
      id: "0",
      profile_id: "0",
      name: "Invité",
      surname: "@Invit",
      picture: "default_profile.png",
      group_id: 0,
      qr_code: "",
      group: {
        name: "Normal",
        picture: "",
        id: 0,
      },
      description:
        "Attention : vous perdrez vos captures à la fermeture de l'application. Avec un compte invité, vous ne pouvez pas être capturé, mais vous pouvez scanner d'autres ensaiens.",
    },
    url_img:
      "https://scontent-cdg2-1.xx.fbcdn.net/v/t1.18169-9/10892002_1605150383037870_8989629625003691553_n.jpg?_nc_cat=108&ccb=1-6&_nc_sid=09cbfe&_nc_ohc=TYBO_4MvzDUAX-TFHll&_nc_ht=scontent-cdg2-1.xx&oh=00_AT-XgPCEqik8iPQsrlaB0wWjal6zweelny51rHPuRrFV5A&oe=62A5959D",
    url: "http://51.210.243.221:8000",
  },
  getters: {
    profiles({ state }) {
      return state.profiles;
    },
    data_type({ state }) {
      return state.data_type;
    },
    dark({ state }) {
      return state.dark;
    },
    color({ state }) {
      return state.color;
    },
    coltext1({ state }) {
      return state.coltext1;
    },
    coltext2({ state }) {
      return state.coltext2;
    },
  },
  actions: {
    addprofile({ state }, profile) {
      state.profiles = [...state.profiles, profile];
    },
    changeColor({ state }, color) {
      state.color = color;
    },
    changeDark({ state }, dark) {
      state.dark = dark;
    },
  },
});
export default store;
