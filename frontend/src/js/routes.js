import HomePage from "../pages/home.jsx";
import AboutPage from "../pages/about.jsx";
import DexPage from "../pages/dex.jsx";
import ProfilePage from "../pages/profile.jsx";
import MyProfilePage from "../pages/myprofile.jsx";
import ContactPage from "../pages/contact.jsx";
import ScannerPage from "../pages/scanner.jsx";
import MycodePage from "../pages/myCode.jsx";
import ProfileeditorPage from "../pages/profileEditor.jsx";
import LegalMentions from "../pages/legal-mentions.jsx";
import rankingsPage from "../pages/rankings.jsx";
import OptionsPage from "../pages/options.jsx";
import loginPage from "../pages/loginscreen.jsx"

import RequestAndLoad from "../pages/request-and-load.jsx";
import NotFoundPage from "../pages/404.jsx";

var routes = [
  {
    path: "/",
    component: HomePage,
  },
  {
    path: "/options/",
    component: OptionsPage,
  },
  {
    path: "/about/",
    component: AboutPage,
  },
  {
    path: "/contact/",
    component: ContactPage,
  },
  {
    path: "/dex/",
    component: DexPage,
  },
  {
    path: "/profile/:id/",
    component: ProfilePage,
  },
  {
    path: "/MyProfile/",
    component: MyProfilePage,
  },
  {
    path: "/profileeditor/",
    component: ProfileeditorPage,
  },
  {
    path: "/scanner/",
    component: ScannerPage,
  },
  {
    path: "/mycode/",
    component: MycodePage,
  },
  {
    path: "/legal/",
    component: LegalMentions,
  },
  {
    path: "/rankings/",
    component: rankingsPage,
  },
  {
    path: "/login",
    component: loginPage,
  },
  {
    path: "/request-and-load/user/:userId/",
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: "Vladimir",
          lastName: "Kharlampidi",
          about: "Hello, i am creator of Framework7! Hope you like it!",
          links: [
            {
              title: "Framework7 Website",
              url: "http://framework7.io",
            },
            {
              title: "Framework7 Forum",
              url: "http://forum.framework7.io",
            },
          ],
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            },
          }
        );
      }, 1000);
    },
  },
  {
    path: "(.*)",
    component: NotFoundPage,
  },
];

export default routes;
