# ENSAdex

"Bien le bonjour, bienvenue dans le monde magique de l'ENSAI ! Mon nom est Jougaud. Les gens souvent m'appellent le Prof ENSAI. Ce monde est peuplé de créatures du nom d'ENSAIENS. Pour certains, les ENSAIENS sont des animaux domestiques, pour d'autres, ils sont un moyen de combattre. Pour ma part... L'étude des ENSAIENS est ma profession.

J'ai une faveur à te demander. Voici mon invention... L'ENSAdex ! Il enregistre les informations sur les ENSAIENS rencontrés ou capturés! C'est comme une encyclopédie! Faire un guide complet sur les ENSAIENS de toute l'école ... C'est mon rêve! Mais je suis trop vieux maintenant! C'est pourquoi je veux que vous terminiez mon travail! Allez, roulez jeunesse! Que la grande quête des ENSAIENS commence!"

## Installation

Télécharger l'application mobile ENSAdex sur le playstore ou l'apple store

## Usage

Scanne le qr code d'un ENSAIEN pour l'ajouter à ton ENSAdex ! A toi des les collecter tous. Tu peux consulter les ENSAIENS capturés dans la sexion ENSAdex de l'application. Tu peux également afficher ton qr-code afin de le partager à d'autres ENSAIENS !

Toute ENSAIENS possède un type : normal, Empire Constrastat, Prostats, Sexion Datassaut ... Tu peux classer les ENSAIENS par type afin de savoir combien il t'en manque.

## Contributing
Nous apprécions toute proposition de contribution sur ce git. N'hésite pas à prendre contact avec le professeur Jougaud pour discuter des idées d'amélioration.


## License
[GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.fr.html)
