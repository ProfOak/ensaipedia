# Hello world

How to setup the server :

Create a .env file from the .env.example file (app/config). Pour le moment t'as rien a modifié j'ai tout mis direct
Crée deux répertoires dans backend: "pictures" et "db_saves"
Dans pictures, crée deux répertoires: "group" et "profile"
Dans chacun de ces deux repertoires, ajoute un jpeg par defaut, tant que le nom commence par "default" ce sera bon

```
cd backend
pip install -r requirements.txt
python -m app.main
``` 

J'ai retiré toute authentification pour que ce soit plus simple pour toi à tester
Si tu veux vérifier que l'authentification marche, tu peux décommenter la ligne "dependencies" dans les routers (par exemple profile_router, ligne 20)

Normalement toutes les lectures fonctionnent, la création est basique, sauf pour les profils qui nécéssitent de post du form-data.
De toute façon si tu testes depuis docs tu chopes le curl qui doit pas être un enfer à traduire en js https://curlconverter.com/