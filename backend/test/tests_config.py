import pytest
from fastapi.testclient import TestClient
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from app.setup.api_setup import app
from app.setup.api_dependencies import get_session
from app.models.user_models import User
from app.utils.hash_utils import get_password_hash


@pytest.fixture(name="session")
def session_fixture():
    engine = create_engine(
        "sqlite://",
        connect_args={"check_same_thread": False},
        poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        user = User(username="admin",
                    hashed_password=get_password_hash("admin"))
        session.add(user)
        session.commit()
        yield session


@pytest.fixture(name="client")
def client_fixture(session: Session):
    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()


@pytest.fixture(name="login")
def login_fixture(client: TestClient):
    headers = {
        "accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    data = ("grant_type=&username=admin&password=admin&scope=&"
            "client_id=&client_secret=")
    req = client.post(
        "/token",
        headers=headers,
        data=data
    )
    response = req.json()
    login = {
        "Authorization": "Bearer " + response["access_token"]
    }
    yield login
