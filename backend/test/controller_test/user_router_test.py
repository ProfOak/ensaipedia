from typing import Any
from app.models.user_models import User
from app.models.profile_models import Profile
from fastapi.testclient import TestClient
from sqlmodel import Session, select

from ..tests_config import (session_fixture,  # noqa F401
                            client_fixture,  # noqa F401
                            login_fixture)  # noqa F401


# Tests for creating a user
def test_create_complete_user(session: Session,
                              client: TestClient,
                              login: dict[str, Any]):
    response = client.post(
        "/users/",
        headers=login,
        json={
            "username": "exampleusername",
            "password": "examplepassword",
            "profile_id": "pid"
        }
    )
    assert response.status_code == 200

    user = response.json()
    assert user["username"] == "exampleusername"
    assert isinstance(user["profile_id"], str)
    assert isinstance(user["id"], int)

    db_user = session.exec(
        select(User).where(User.username == "exampleusername")).one()
    assert isinstance(db_user.hashed_password, str)
    assert db_user.username == user["username"]
    assert db_user.profile_id == user["profile_id"]
    assert db_user.id == user["id"]


def test_create_user_invalid(client: TestClient,
                             login: dict[str, Any]):
    # profile_id has an invalid type
    response = client.post(
        "/users/",
        headers=login,
        json={
            "username": "exampleusername",
            "password": {"name": 2},
            "profile_id": "id"
        },
    )
    assert response.status_code == 422


def test_create_user_incomplete(client: TestClient,
                                login: dict[str, Any]):
    # Missing password parameter
    response = client.post(
        "/users/",
        headers=login,
        json={
            "username": "exampleusername",
            "profile_id": 1
        },
    )
    assert response.status_code == 422


# Tests for reading users
def test_read_users(session: Session,
                    client: TestClient,
                    login: dict[str, Any]):
    user_1 = User(username="example 1",
                  hashed_password="hpassword1",
                  profile_id="pid")
    user_2 = User(username="example 2",
                  hashed_password="hpassword2")
    session.add(user_1)
    session.add(user_2)
    session.commit()

    response = client.get(
        "/users/",
        headers=login
    )
    users = response.json()

    assert response.status_code == 200

    assert len(users) == 3
    assert users[1]["username"] == user_1.username
    assert users[1]["profile_id"] == user_1.profile_id
    assert isinstance(users[1]["id"], int)
    assert users[2]["username"] == user_2.username
    assert users[2]["profile_id"] is None
    assert isinstance(users[2]["id"], int)


def test_read_user_with_profile(session: Session,
                                client: TestClient,
                                login: dict[str, Any]):
    profile = Profile(
        name="John Doe",
        surname="J",
        description="Unknown man",
        picture="fake_picture_id",
        id="fake_profile_uuim_id"
    )
    session.add(profile)
    session.commit()
    session.refresh(profile)

    user = User(
        username="johndoe",
        hashed_password="fakepassword",
        profile_id=profile.id
    )
    session.add(user)
    session.commit()
    session.refresh(user)

    response = client.get(
        f"/users/{user.id}",
        headers=login
    )
    user_data = response.json()

    assert response.status_code == 200
    assert user_data["username"] == user.username
    assert user_data["profile_id"] == user.profile_id
    assert isinstance(user_data["id"], int)
    assert user_data["profile"] is not None


# test for updating users
def test_update_user(session: Session,
                     client: TestClient,
                     login: dict[str, Any]):
    user = User(username="exampleusername",
                hashed_password="fake_hashed_pwd",
                profile_id="fake_profile_id")
    session.add(user)
    session.commit()

    response = client.patch(
        f"/users/{user.id}",
        headers=login,
        json={
            "username": "newusername",
            "password": "password"
        }
    )

    user_data = response.json()

    assert response.status_code == 200
    assert user_data["username"] == "newusername"
    assert isinstance(user_data["id"], int)
    assert user_data["profile_id"] == user.profile_id


# tests for deleting users
def test_delete_user(session: Session,
                     client: TestClient,
                     login: dict[str, Any]):
    user_1 = User(username="Deadpond",
                  hashed_password="fake_hashed_pwd",
                  profile_id="fake_profile_id")
    session.add(user_1)
    session.commit()

    response = client.delete(
        f"/users/{user_1.id}",
        headers=login
    )

    hero_in_db = session.get(User, user_1.id)

    assert response.status_code == 200

    assert hero_in_db is None
