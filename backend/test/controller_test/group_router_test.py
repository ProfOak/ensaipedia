from typing import Any
from fastapi.testclient import TestClient
from sqlmodel import Session, select

from app.models.group_models import Group
from app.utils.default_pics import get_default_group_picture_id

from ..tests_config import (session_fixture,  # noqa F401
                            client_fixture,  # noqa F401
                            login_fixture)  # noqa F401


def test_create_group_no_pic(client: TestClient,
                               login: dict[str, Any]):
    response = client.post(
        "/groups/",
        headers=login,
        data={
            "name": "examplegroupname"
        }
    )
    assert response.status_code == 200

    group = response.json()
    assert group["name"] == "examplegroupname"
    assert group["picture"] == get_default_group_picture_id()
    assert 