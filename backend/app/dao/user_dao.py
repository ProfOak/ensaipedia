"""Database access for users"""
from sqlmodel import Session, select
from fastapi import HTTPException

from ..models.user_models import User
from ..models.profile_models import Profile


class UserDao:

    def __init__(self, session: Session):
        self.session = session

    def create_user(self, user: User):
        db_user = User.from_orm(user)  # Just to checl if it fits
        self.session.add(db_user)
        self.session.commit()
        self.session.refresh(db_user)
        return db_user

    def read_users(self, offset: int, limit: int):
        statement = select(User).offset(offset).limit(limit)
        users = self.session.exec(statement).all()
        return users

    def read_user(self, user_id: int):
        user = self.session.get(User, user_id)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        return user

    def update_user(self, user_id: int, user: User):  # already hashed_pwd
        db_user = self.session.get(User, user_id)
        if not db_user:
            raise HTTPException(status_code=404, detail="User not found")

        user_data = user.dict(exclude_unset=True, exclude_none=True)
        user_data["hashed_password"] = user_data.pop("password")
        for key, value in user_data.items():
            setattr(db_user, key, value)

        self.session.add(db_user)
        self.session.commit()
        self.session.refresh(db_user)

        return db_user

    def delete_user(self, user_id: int):
        user = self.session.get(User, user_id)
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        self.session.delete(user)
        self.session.commit()
        return user

    # Switching to alternative version (i.e not with user_id)

    def read_user_by_username(self, username: str) -> User:
        statement = select(User).where(User.username == username)
        user = self.session.exec(statement).one()
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        return user

    def add_profile_to_dex(self, user_id: int, profile_id: int):
        user = self.session.get(User, user_id)
        profile = self.session.get(Profile, profile_id)
        user.dex.append(profile)
        self.session.add(user)
        self.session.commit()
        self.session.refresh(user)
        return user
