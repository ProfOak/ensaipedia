"""Database access for profiles"""
from sqlmodel import Session, select
from fastapi import HTTPException

from ..utils.uuid_utils import get_uuid
from ..models.profile_models import Profile


class ProfileDao:

    def __init__(self, session: Session):
        self.session = session

    def create_profile(self, profile: Profile):
        db_profile = Profile.from_orm(profile)
        db_profile.id = get_uuid()
        self.session.add(db_profile)
        self.session.commit()
        self.session.refresh(db_profile)
        return db_profile

    def read_profiles(self, offset: int, limit: int):
        statement = select(Profile).offset(offset).limit(limit)
        profiles = self.session.exec(statement).all()
        return profiles

    def read_profile(self, profile_id: str):
        profile = self.session.get(Profile, profile_id)
        if not profile:
            raise HTTPException(status_code=404, detail="Profile not found")
        return profile

    def update_profile(self, profile_id: str, profile: Profile):
        db_profile = self.session.get(Profile, profile_id)
        if not db_profile:
            raise HTTPException(status_code=404, detail="Profile not found")

        profile_data = profile.dict(exclude_unset=True, exclude_none=True)
        for key, value in profile_data.items():
            setattr(db_profile, key, value)

        self.session.add(db_profile)
        self.session.commit()
        self.session.refresh(db_profile)

        return db_profile

    def delete_profile(self, profile_id: str):
        profile = self.session.get(Profile, profile_id)
        if not profile:
            raise HTTPException(status_code=404, detail="Profile not found")

        self.session.delete(profile)
        self.session.commit()
        return profile
