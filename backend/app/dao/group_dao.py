"""Database access for groups"""
from sqlmodel import Session, select
from fastapi import HTTPException

from ..models.group_models import Group


class GroupDao:

    def __init__(self, session: Session):
        self.session = session

    def create_group(self, group: Group):
        db_group = Group.from_orm(group)
        self.session.add(db_group)
        self.session.commit()
        self.session.refresh(db_group)
        return db_group

    def read_groups(self, offset: int, limit: int):
        statement = select(Group).offset(offset).limit(limit)
        groups = self.session.exec(statement).all()
        return groups

    def read_group(self, group_id: int):
        group = self.session.get(Group, group_id)
        if not group:
            raise HTTPException(status_code=404, detail="Group not found")
        return group

    def update_group(self, group_id: int, group: Group):
        db_group = self.session.get(Group, group_id)
        if not db_group:
            raise HTTPException(status_code=404, detail="Group not found")

        group_data = group.dict(exclude_unset=True, exclude_none=True)
        for key, value in group_data.items():
            setattr(db_group, key, value)

        self.session.add(db_group)
        self.session.commit()
        self.session.refresh(db_group)

        return db_group

    def delete_group(self, group_id: int):
        group = self.session.get(Group, group_id)
        if not group:
            raise HTTPException(status_code=404, detail="Group not found")

        self.session.delete(group)
        self.session.commit()
        return group
