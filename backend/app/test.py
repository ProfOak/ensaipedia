import requests
from .setup.database_setup import delete_db


def post_profile():
    headers = {
        "accept": "application/json"
        # "Content-Type": "multipart/form-data"
    }
    files = {
        'file': ('test_pic.jpg;type=image/jpeg',
                 open('backend/app/test_pic.jpg', 'rb')),
        'name': (None, 'jeanpaul'),
        "surname": (None, 'pdpdpd'),
        "description": (None, "tema la description"),
        "group_id": (None, '3')
    }
    req = requests.post("http://127.0.0.1:8000/profiles/",
                        headers=headers,
                        files=files)
    if req.status_code == 200:
        return req.json()
    else:
        raise NameError("Wrong status code")


def post_user():
    req = requests.post(
        "http://127.0.0.1:8000/users/",
        json={
            "username": "exampleusername",
            "profile_id": 1
        }
    )
    print(req.status_code)


def login():
    headers = {
        "accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    }
    data = ("grant_type=&username=admin&password=admin&scope=&"
            "client_id=&client_secret=")
    req = requests.post(
        "http://127.0.0.1:8000/token",
        headers=headers,
        data=data
    )
    response = req.json()
    login = {
        "Authorization": "Bearer " + response["access_token"]
    }
    return login


def read_user():
    headers = login()
    req = requests.get(
        "http://127.0.0.1:8000/users/",
        headers=headers
    )
    return req.json()


delete_db()
