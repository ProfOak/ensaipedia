"""This is where we configure all the API:
Dependencies, title, include all routers, etc.
"""
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

from ..setup.database_setup import create_db_and_tables, init_admin, delete_db
from ..controller import (
    user_router,
    profile_router,
    group_router,
    token_router,
    player_router
)
from ..config.api_config import APIConfig
from ..utils.db_utils import save_db

GROUP_PICTURES_DIR = APIConfig().group_pic_dir
PROFILE_PICTURES_DIR = APIConfig().profile_pic_dir
QR_CODES_DIR = APIConfig().qr_code_dir

tags_megadata = [
    {
        "name": "Authentication"
    },
    {
        "name": "Non Admin"
    },
    {
        "name": "Users"
    },
    {
        "name": "Profiles"
    },
    {
        "name": "Groups"
    }
]

app = FastAPI(
    title="EnsaiPedia",
    version="0.0.2",
    description="Catch'em all!"
)

# In case of need

""" origins = [
    "http://localhost:3000",
] """


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=False,
    allow_methods=['*'],
    allow_headers=['*']
)


@app.on_event("startup")
def on_startup():
    create_db_and_tables()
    init_admin()


@app.on_event("shutdown")
def on_shutdown():
    save_db()
    #delete_db()


app.include_router(token_router.router, tags=["Authentication"])

@app.get("/health",
        summary="Check server health",
        tags=["Health"])
def check_health():
    return {'health': 'Everything seems fine'}

app.include_router(player_router.router, tags=["Non Admin"])
app.include_router(user_router.router, tags=["Users"])
app.include_router(profile_router.router, tags=["Profiles"])
app.include_router(group_router.router, tags=["Groups"])

app.mount("/group_pictures",
          StaticFiles(directory=GROUP_PICTURES_DIR),
          name="group_pictures")
app.mount("/profile_pictures",
          StaticFiles(directory=PROFILE_PICTURES_DIR),
          name="profile_pictures")
app.mount("/qr_codes",
          StaticFiles(directory=QR_CODES_DIR),
          name="qr_codes")