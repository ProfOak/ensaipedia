"""
This is where we store information regarding the database configuration
Aka this is where we will import the engine
"""
import os
from sqlmodel import SQLModel, Session, create_engine

from ..models.user_models import User  # noqa: F401
from ..models.profile_models import Profile  # noqa: F401
from ..models.group_models import Group  # noqa: F401
from ..models.user_profile_link import UserProfileLink  # noqa: F401
from ..config.database_config import DBConfig
from ..utils.hash_utils import get_password_hash

DB_NAME = DBConfig().db_name
SQLITE_URL = DBConfig().db_url
ECHO_VAL = DBConfig().echo
SAME_THREAD_VAL = DBConfig().same_thread

engine = create_engine(
    SQLITE_URL,
    echo=ECHO_VAL,
    connect_args={"check_same_thread": SAME_THREAD_VAL})


def create_db_and_tables():
    SQLModel.metadata.create_all(engine)


def init_admin():
    try :
        with Session(engine) as session:
            user = User(username="admin",
                        hashed_password=get_password_hash("admin"),
                        is_admin=True)
            session.add(user)
            session.commit()
    except Exception:
        pass


def delete_db():
    os.remove(DB_NAME)
