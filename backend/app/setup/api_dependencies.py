"""Here we define all the depencies used by the API"""
from sqlmodel import Session
from .database_setup import engine
from fastapi.security import OAuth2PasswordBearer
from fastapi import HTTPException, status, Depends
from jose import jwt, JWTError
from ..config.authentication_config import AuthConfig
from ..models.token_models import TokenData
from ..models.user_models import User
from ..service.user_service import UserService

SECRET_KEY = AuthConfig().secret_key
ALGORITHM = AuthConfig().algorithm

oauth2scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_session():
    with Session(engine) as session:
        yield session


def get_current_user(token: str = Depends(oauth2scheme),
                     session: Session = Depends(get_session)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"}
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception

    user = UserService(session).read_user_by_username(token_data.username)
    if user is None:
        raise credentials_exception
    return user

async def get_current_admin(current_user: User = Depends(get_current_user)):
    if not current_user.is_admin:
        raise HTTPException(status_code=401, detail="Not Allowed")
    return current_user