import uuid


def get_uuid():
    u = uuid.uuid4()
    return u.hex


def get_uuid_int():
    u = uuid.uuid4()
    return u.int
