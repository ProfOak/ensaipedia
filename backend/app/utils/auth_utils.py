"""Functions related to authentication by token"""
from typing import Optional
from datetime import datetime, timedelta
from jose import jwt

from ..config.authentication_config import AuthConfig

SECRET_KEY = AuthConfig().secret_key
ALGORITHM = AuthConfig().algorithm


def create_access_token(data: dict,
                        expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()

    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)

    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt
