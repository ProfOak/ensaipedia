import sqlite3
from sqlite3 import Error
from xlsxwriter.workbook import Workbook
from datetime import datetime

from app.config.database_config import DBConfig

DB_NAME = DBConfig().db_name
SAVE_DIR = DBConfig().save_dir


def create_save_name():
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
    return f"{SAVE_DIR}/{dt_string}.xlsx"


def get_tables_name(db_name: str):
    try:
        table_names = []
        conn = sqlite3.connect(db_name)
        cursor = conn.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        for table in cursor.fetchall():
            table_names.append(table[0])
        return table_names

    except Error as e:
        print(e)

    finally:
        conn.close()


def save_db():
    workbook = Workbook(create_save_name())
    tables = get_tables_name(DB_NAME)

    try:
        conn = sqlite3.connect(DB_NAME)
        cursor = conn.cursor()

        for table in tables:
            worksheet = workbook.add_worksheet(name=table)
            cursor.execute(f"SELECT * FROM \"{table}\"")
            values = cursor.fetchall()
            for i, row in enumerate(values):
                for j, _ in enumerate(row):
                    worksheet.write(i, j, row[j])

    except Error as e:
        print(e)

    finally:
        conn.close()

    workbook.close()
