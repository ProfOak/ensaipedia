import shutil
import os

from ..config.api_config import APIConfig


def write_jpg(path, file, filename):
    with open(f"{path}/{filename}", 'wb') as buffer:
        shutil.copyfileobj(file, buffer)
    file.close()


def get_extension(filename):
    return os.path.splitext(filename)[1]


def save_group_picture(picture, name):
    DIRECTORY = APIConfig().group_pic_dir
    with open(f"{DIRECTORY}/{name}", "wb") as buffer:
        shutil.copyfileobj(picture, buffer)
    picture.close()


def save_profile_picture(picture, name):
    DIRECTORY = APIConfig().profile_pic_dir
    with open(f"{DIRECTORY}/{name}", "wb") as buffer:
        shutil.copyfileobj(picture, buffer)
    picture.close()
