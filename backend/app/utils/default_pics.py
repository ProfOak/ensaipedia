import os
from ..config.api_config import APIConfig


PATH_TO_DEFAULT_GROUP = APIConfig().group_pic_dir
PATH_TO_DEFAULT_PROFILE = APIConfig().profile_pic_dir


def get_default_group_picture_id():
    for file in os.listdir(PATH_TO_DEFAULT_GROUP):
        if file.startswith("default"):
            return file


def get_default_profile_picture_id():
    for file in os.listdir(PATH_TO_DEFAULT_PROFILE):
        if file.startswith("default"):
            return file
