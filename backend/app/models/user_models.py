"""Defining all the models relative to users"""
from typing import TYPE_CHECKING, Optional, List
from sqlmodel import SQLModel, Field, Relationship, UniqueConstraint

from ..models.user_profile_link import UserProfileLink

if TYPE_CHECKING:
    from ..models.profile_models import Profile


class UserBase(SQLModel):

    username: str = Field(index=True)
    profile_id: Optional[str] = Field(foreign_key="profile.id")
    is_admin: bool = Field(default=False)


class User(UserBase, table=True):
    __table_args__ = (UniqueConstraint("username"),)

    id: Optional[int] = Field(default=None, primary_key=True)
    hashed_password: str

    profile: Optional["Profile"] = Relationship(back_populates="user")
    dex: List["Profile"] = Relationship(back_populates="users",
                                        link_model=UserProfileLink)


class UserCreate(UserBase):
    password: str


class UserRead(UserBase):
    id: int


# Don't now if we want to update profile_id, I don't think so
class UserUpdate(UserBase):
    username: Optional[str] = None
    password: Optional[str] = None
    profile_id: Optional[str] = None
