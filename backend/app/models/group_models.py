"""Models related to groups"""
from typing import TYPE_CHECKING, Optional, List
from sqlmodel import SQLModel, Field, Relationship, UniqueConstraint
from fastapi import Form

if TYPE_CHECKING:
    from ..models.profile_models import Profile


class GroupBase(SQLModel):
    name: str = Field(index=True)
    picture: Optional[str] = None


class Group(GroupBase, table=True):
    __table_args__ = (UniqueConstraint("name"),)

    id: Optional[int] = Field(default=None, primary_key=True)
    picture: str

    profiles: List["Profile"] = Relationship(back_populates="group")


class GroupCreate(GroupBase):

    def __init__(self, name: str = Form(...)):
        super().__init__(name=name)


class GroupRead(GroupBase):
    id: int
    picture: str


class GroupUpdate(GroupBase):
    name: Optional[str] = None

    def __init__(self, name: str = Form(None)):
        super().__init__(name=name)
