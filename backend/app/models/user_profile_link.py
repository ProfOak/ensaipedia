from typing import Optional
from sqlmodel import SQLModel, Field


class UserProfileLink(SQLModel, table=True):

    user_id: Optional[int] = Field(default=None,
                                   foreign_key="profile.id",
                                   primary_key=True)

    profile_id: Optional[int] = Field(default=None,
                                      foreign_key="user.id",
                                      primary_key=True)
