"""We define the models useful for working with relationships
That is : all the FASTAPI reading models
"""
from typing import Optional, List

from ..models.user_models import UserRead
from ..models.profile_models import ProfileRead
from ..models.group_models import GroupRead


class GroupReadWithProfiles(GroupRead):
    profiles: List[ProfileRead] = []


class ProfileReadWithGroup(ProfileRead):
    group: Optional[GroupRead] = None


class ProfileReadWithUser(ProfileRead):
    user: Optional[UserRead] = None


class ProfileReadWithOwners(ProfileRead):
    users: List[UserRead] = None


class ProfileReadWithUserAndOwners(ProfileReadWithOwners, ProfileReadWithUser):
    pass


class ProfileReadWithAll(ProfileReadWithGroup, ProfileReadWithUserAndOwners):
    pass


class UserReadWithProfile(UserRead):
    profile: Optional[ProfileRead] = None


class UserReadWithProfileAndGroup(UserRead):
    profile: Optional[ProfileReadWithGroup] = None


class UserReadWithDex(UserRead):
    dex: List[ProfileRead] = []


class UserReadWithProfileAndDex(UserReadWithDex, UserReadWithProfile):
    pass
