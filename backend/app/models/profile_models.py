"""Models related to profiles"""
from typing import TYPE_CHECKING, Optional, List
from sqlmodel import SQLModel, Field, Relationship
from fastapi import Form

from ..models.user_profile_link import UserProfileLink

if TYPE_CHECKING:
    from ..models.user_models import User
    from ..models.group_models import Group


class ProfileBase(SQLModel):

    name: Optional[str] = Field(index=True)
    surname: Optional[str]
    description: Optional[str]
    # picture: Optional[str] = None
    # qr_code: Optional[str] = None

    group_id: Optional[int] = Field(default=None, foreign_key="group.id")

class ProfileInt(ProfileBase):
    picture: Optional[str] = None
    qr_code: Optional[str] = None


class Profile(ProfileBase, table=True):

    id: Optional[str] = Field(primary_key=True)
    picture: str
    qr_code: str

    group: Optional["Group"] = Relationship(back_populates="profiles")
    user: Optional["User"] = Relationship(back_populates="profile")
    users: List["User"] = Relationship(back_populates="dex",
                                       link_model=UserProfileLink)


class ProfileRead(ProfileBase):
    id: str
    picture: str
    qr_code: str


class ProfileCreate(ProfileInt):
    name: Optional[str] = None
    surname: Optional[str] = None
    description: Optional[str] = None
    group_id: Optional[int] = None
    picture: Optional[str] = None

    def __init__(self,
                 name: str = Form(...),
                 surname: str = Form(None),
                 description: str = Form(None),
                 group_id: int = Form(None)):

        super().__init__(name=name,
                         surname=surname,
                         description=description,
                         group_id=group_id)

class outProfileCreate(ProfileBase):
    pass

class ProfileUpdate(ProfileBase):
    name: Optional[str] = None
    surname: Optional[str] = None
    description: Optional[str] = None
    group_id: Optional[int] = None


class outProfileUpdate(ProfileBase):
    name: Optional[str] = None
    surname: Optional[str] = None
    description: Optional[str] = None
    group_id: Optional[int] = None

    def __init__(self,
                 name: str = Form(None),
                 surname: str = Form(None),
                 description: str = Form(None),
                 group_id: int = Form(None)):

        super().__init__(name=name,
                         surname=surname,
                         description=description,
                         group_id=group_id)
