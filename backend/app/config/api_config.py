import os
from dotenv import load_dotenv

from ..utils.singleton import Singleton

load_dotenv()


class APIConfig(metaclass=Singleton):
    def __init__(self):
        self.__host = os.getenv("API_HOST")
        self.__port = os.getenv("API_PORT")
        self.__reload = os.getenv("RELOAD")
        self.__group_pic_dir = os.getenv("GROUP_PICTURES_DIR")
        self.__profile_pic_dir = os.getenv("PROFILE_PICTURES_DIR")
        self.__qr_code_dir = os.getenv("QR_CODES_DIR")

    @property
    def host(self):
        return self.__host

    @property
    def port(self):
        return int(self.__port)

    @property
    def reload(self):
        return self.__reload == "True"

    @property
    def group_pic_dir(self):
        return self.__group_pic_dir

    @property
    def profile_pic_dir(self):
        return self.__profile_pic_dir

    @property
    def qr_code_dir(self):
        return self.__qr_code_dir
