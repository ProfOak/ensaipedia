import os
from dotenv import load_dotenv

from ..utils.singleton import Singleton

load_dotenv()


class DBConfig(metaclass=Singleton):
    def __init__(self):
        self.__db_name = os.getenv("DATABASE_NAME")
        self.__db_url = os.getenv("DATABASE_URL")
        self.__echo = os.getenv("ECHO")
        self.__same_thread = os.getenv("SAME_THREAD")
        self.__db_saves = os.getenv("DATABASE_SAVES_DIR")

    @property
    def db_name(self):
        return self.__db_name

    @property
    def db_url(self):
        return self.__db_url

    @property
    def echo(self):
        return self.__echo == "True"

    @property
    def same_thread(self):
        return self.__same_thread == "True"

    @property
    def save_dir(self):
        return self.__db_saves
