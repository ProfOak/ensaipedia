import os
from dotenv import load_dotenv

from ..utils.singleton import Singleton

load_dotenv()


class AuthConfig(metaclass=Singleton):
    def __init__(self):
        self.__key = os.getenv("SECRET_KEY")
        self.__algorithm = os.getenv("ALGORITHM")
        self.__token_exp = os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")

    @property
    def secret_key(self):
        return self.__key

    @property
    def algorithm(self):
        return self.__algorithm

    @property
    def token_expires_minutes(self):
        return int(self.__token_exp)
