"""
This is where we will start the API, and therefore the whole backend server
"""
import uvicorn

from .setup.api_setup import app
from .config.api_config import APIConfig

HOST = APIConfig().host
PORT = APIConfig().port


if __name__ == "__main__":
    uvicorn.run(app, host=HOST, port=PORT)
