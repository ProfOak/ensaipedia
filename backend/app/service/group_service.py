""""Operations betwenn router and dao for groups"""
from typing import Optional
from fastapi import UploadFile
from sqlmodel import Session

from ..dao.group_dao import GroupDao
from ..models.group_models import GroupCreate, GroupUpdate
from ..utils.default_pics import get_default_group_picture_id
from ..utils.uuid_utils import get_uuid
from ..utils.file_utills import get_extension, save_group_picture


class GroupService:

    def __init__(self, session: Session):
        self.session = session

    def create_group(self,
                     group: GroupCreate,
                     file: Optional[UploadFile] = None):
        if file is None:
            picture_id = get_default_group_picture_id()
        else:
            picture_id = get_uuid() + get_extension(file.filename)
            save_group_picture(file.file, picture_id)

        group.picture = picture_id
        return GroupDao(self.session).create_group(group)

    def read_groups(self, offset: int, limit: int):
        return GroupDao(self.session).read_groups(offset, limit)

    def read_group(self, group_id: int):
        return GroupDao(self.session).read_group(group_id)

    def update_group(self,
                     group_id: int,
                     group: GroupUpdate,
                     file: Optional[UploadFile] = None):

        if file is not None:
            db_group = GroupDao(self.session).read_group(group_id)
            if db_group.picture == get_default_group_picture_id():
                picture_id = get_uuid() + get_extension(file.filename)
            else:
                picture_id = db_group.picture
            save_group_picture(file.file, picture_id)
            group.picture = picture_id

        return GroupDao(self.session).update_group(group_id, group)

    def delete_group(self, group_id: int):
        return GroupDao(self.session).delete_group(group_id)
