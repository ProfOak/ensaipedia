"""Operations betwenn controller and dao regarding users
THAT might be where we hash on creation
"""
from sqlmodel import Session

from ..dao.user_dao import UserDao
from ..models.user_models import User, UserCreate, UserUpdate
from ..utils.hash_utils import verify_password, get_password_hash


class UserService:

    def __init__(self, session: Session):
        self.session = session

    def create_user(self, user: UserCreate):
        db_user = User.parse_obj(user)
        db_user.hashed_password = get_password_hash(user.password)
        return UserDao(self.session).create_user(db_user)

    def read_users(self, offset: int, limit: int):
        return UserDao(self.session).read_users(offset, limit)

    def read_user(self, user_id: int):
        return UserDao(self.session).read_user(user_id)

    def update_user(self, user_id: int, user: UserUpdate):
        hashed_password = get_password_hash(user.password)
        user.password = hashed_password
        return UserDao(self.session).update_user(user_id, user)

    def delete_user(self, user_id: int):
        return UserDao(self.session).delete_user(user_id)

    def read_user_by_username(self, username: str):
        return UserDao(self.session).read_user_by_username(username)

    def authenticate_user(self, username: str, password: str):
        user = UserService(self.session).read_user_by_username(username)
        if not user:
            return False
        if not verify_password(password, user.hashed_password):
            return False
        return user

    def add_profile_to_dex(self, user_id: int, profile_id: int):
        return UserDao(self.session).add_profile_to_dex(user_id, profile_id)
