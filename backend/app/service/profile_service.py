""""Operations betwenn reouter and dao for profiles"""
import qrcode
from typing import Optional
from fastapi import UploadFile
from sqlmodel import Session

from ..dao.profile_dao import ProfileDao
from ..models.profile_models import ProfileCreate, ProfileUpdate
from ..utils.default_pics import get_default_profile_picture_id
from ..utils.file_utills import get_extension, save_profile_picture
from ..utils.uuid_utils import get_uuid
from ..config.api_config import APIConfig

QR_CODES_DIR = APIConfig().qr_code_dir


class ProfileService:

    def __init__(self, session: Session):
        self.session = session

    def create_profile(self,
                       profile: ProfileCreate,
                       file: Optional[UploadFile] = None):

        if file is None:
            picture_id = get_default_profile_picture_id()
        else:
            picture_id = get_uuid() + get_extension(file.filename)
            save_profile_picture(file.file, picture_id)

        profile.picture = picture_id
        profile.qr_code = get_uuid()
        db_profile =  ProfileDao(self.session).create_profile(profile)

        profile_id = db_profile.id
        qr_code = db_profile.qr_code
        img = qrcode.make(profile_id)
        img.save(f"{QR_CODES_DIR}/{qr_code}.jpg")

        return db_profile

    def read_profiles(self, offset: int, limit: int):
        return ProfileDao(self.session).read_profiles(offset, limit)

    def read_profile(self, profile_id: str):
        return ProfileDao(self.session).read_profile(profile_id)

    def update_profile(self,
                       profile_id: str,
                       profile: ProfileUpdate,
                       file: Optional[UploadFile] = None):
        if file is not None:
            db_profile = ProfileDao(self.session).read_profile(profile_id)
            if db_profile.picture == get_default_profile_picture_id():
                picture_id = get_uuid() + get_extension(file.filename)
            else:
                picture_id = db_profile.picture
            save_profile_picture(file.file, picture_id)
            profile.picture = picture_id

        return ProfileDao(self.session).update_profile(profile_id, profile)

    def update_picture(self,
                       profile_id: str,
                       file: UploadFile):
        db_profile = ProfileDao(self.session).read_profile(profile_id)
        if db_profile.picture == get_default_profile_picture_id():
            picture_id = get_uuid() + get_extension(file.filename)
        else:
            picture_id = db_profile.picture
        save_profile_picture(file.file, picture_id)
        db_profile.picture = picture_id

        return ProfileDao(self.session).update_profile(profile_id, db_profile)

    def delete_profile(self, profile_id: str):
        return ProfileDao(self.session).delete_profile(profile_id)
