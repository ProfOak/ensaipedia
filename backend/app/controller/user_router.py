"""Add all path operations relative to user
INCLUDES: authenticate_user(username, password)"""
from typing import List
from sqlmodel import Session
from fastapi import APIRouter, Depends, Query

from ..models.user_models import User, UserCreate, UserRead, UserUpdate
from ..models.profile_models import ProfileRead
from ..models.relationship_models import UserReadWithProfile, UserReadWithDex, UserReadWithProfileAndDex
from ..service.user_service import UserService
from ..setup.api_dependencies import get_session, get_current_admin


router = APIRouter(
    prefix="/users",
    dependencies=[Depends(get_current_admin)]
)

@router.post("/", response_model=UserRead, summary="Create a user (Admin only)")
def create_user(*,
                session: Session = Depends(get_session),
                user: UserCreate):
    return UserService(session).create_user(user)


@router.get("/", response_model=List[UserRead], summary="Read several users (Admin only)")
def read_users(*,
               session: Session = Depends(get_session),
               offset: int = 0,
               limit: int = Query(default=100, lte=100)):
    return UserService(session).read_users(offset, limit)


@router.get("/by_name/{username}", 
            response_model = UserReadWithProfileAndDex, 
            summary="Get a user info with their profile and dex using their username (Admin only)")
def read_user_by_username(*,
                          session: Session = Depends(get_session),
                          username: str):
    return UserService(session).read_user_by_username(username)


@router.get("/{user_id}", 
            response_model=UserReadWithProfileAndDex,
            summary="Get a user with their profile and dex using their id (Admin only)")
def read_user(*,
              session: Session = Depends(get_session),
              user_id: int):
    return UserService(session).read_user(user_id)


@router.patch("/{user_id}", 
              response_model=UserRead,
              summary="Update a user from their id (Admin only)")
def update_user(*,
                session: Session = Depends(get_session),
                user_id: int,
                user: UserUpdate):
    return UserService(session).update_user(user_id, user)


@router.delete("/{user_id}",
               summary="Delete a user by their id (Admin only)")
def delete_user(*,
                session: Session = Depends(get_session),
                user_id: int):
    user = UserService(session).delete_user(user_id)
    return {
        "deleted_user": {
            "id": user.id,
            "username": user.username
        }
    }


@router.patch("/{user_id}",
             summary="Add a profile to the dex of a user (Admin only)")
def add_profile_to_dex(*,
                       session: Session = Depends(get_session),
                       user_id: int,
                       profile_id: str):
    db_user = UserService(session).add_profile_to_dex(user_id, profile_id)
    user = UserRead.from_orm(db_user)
    profile = ProfileRead.from_orm(db_user.dex[0])
    return {
        "user": user,
        "added_profile": profile
    }
