"""Path operations relative to groups"""
from typing import List, Optional
from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, UploadFile, File

from ..models.group_models import GroupCreate, GroupRead, GroupUpdate
from ..models.relationship_models import GroupReadWithProfiles
from ..service.group_service import GroupService
from ..setup.api_dependencies import get_session, get_current_admin


router = APIRouter(
    prefix="/groups",
    dependencies=[Depends(get_current_admin)]
)


@router.post("/",
             response_model=GroupRead,
             summary="Create a Group (Admin only)")
def create_group(*,
                 session: Session = Depends(get_session),
                 group: GroupCreate = Depends(),
                 file: Optional[UploadFile] = File(None)):
    return GroupService(session).create_group(group, file)


@router.get("/",
            response_model=List[GroupRead],
            summary="Read several groups (Admin only)")
def read_groups(*,
                session: Session = Depends(get_session),
                offset: int = 0,
                limit: int = Query(default=100)):
    return GroupService(session).read_groups(offset, limit)


@router.get("/{group_id}",
            response_model=GroupReadWithProfiles,
            summary="Reag a group by id (Admin only)")
def read_group(*,
               session: Session = Depends(get_session),
               group_id: int):
    return GroupService(session).read_group(group_id)


@router.patch("/{group_id}",
              response_model=GroupRead,
              summary="Update a group by id (Admin only)")
def update_group(*,
                 session: Session = Depends(get_session),
                 group_id: int,
                 group: GroupUpdate = Depends(),
                 file: Optional[UploadFile] = File(None)):
    return GroupService(session).update_group(group_id, group, file)


@router.delete("/{group_id}",
               summary="Delete a group by id (Admin only)")
def delete_group(*,
                 session: Session = Depends(get_session),
                 group_id: int):
    group = GroupService(session).delete_group(group_id)
    return {
        "deleted group": {
            "id": group.id,
            "name": group.name
        }
    }
