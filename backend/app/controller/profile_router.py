"""Path operations relative to profiles"""
from typing import List, Optional
from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, UploadFile, File
import qrcode

from ..models.profile_models import ProfileCreate, ProfileRead, ProfileUpdate
from ..models.user_models import User
from ..models.relationship_models import ProfileReadWithGroup
from ..service.profile_service import ProfileService
from ..setup.api_dependencies import get_session, get_current_admin


router = APIRouter(
    prefix="/profiles",
    dependencies=[Depends(get_current_admin)]
)


@router.post("/",
             response_model=ProfileRead,
             summary="Create a profile (Admin Only)")
async def create_profile(*,
                         session: Session = Depends(get_session),
                         profile: ProfileCreate = Depends(),
                         file: Optional[UploadFile] = File(None)):

    return ProfileService(session).create_profile(profile, file)



@router.get("/",
            response_model=List[ProfileRead],
            summary="Read several profiles (Admin only)")
def read_profiles(*,
                  session: Session = Depends(get_session),
                  offset: int = 0,
                  limit: int = Query(default=100, lte=100)):
    return ProfileService(session).read_profiles(offset, limit)


@router.get("/{profile_id}",
            response_model=ProfileReadWithGroup,
            summary="Read a profile by id (Admin only)")
def read_profile(*,
                 session: Session = Depends(get_session),
                 profile_id: str):
    return ProfileService(session).read_profile(profile_id)


@router.patch("/{profile_id}",
              response_model=ProfileRead,
              summary="Update a profile by id (Admin only)")
def update_profile(*,
                   session: Session = Depends(get_session),
                   profile_id: str,
                   profile: ProfileUpdate = Depends(),
                   file: Optional[UploadFile] = File(None)):
    return ProfileService(session).update_profile(profile_id, profile, file)


@router.delete("/{profile_id}",
               summary="Delete a profile by id (Admin only)")
def delete_profile(*,
                   session: Session = Depends(get_session),
                   profile_id: str):
    profile = ProfileService(session).delete_profile(profile_id)
    return {
        "deleted profile": {
            "id": profile.id,
            "name": profile.name
        }
    }
