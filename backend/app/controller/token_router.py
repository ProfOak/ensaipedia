"""Path operation related to token
AKA login
Here we will not need the user dependency
But session might be needed in order to authenticate user
"""
from datetime import timedelta
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from ..setup.api_dependencies import get_session
from ..config.authentication_config import AuthConfig
from ..utils.auth_utils import create_access_token
from ..service.user_service import UserService
from ..models.token_models import Token

ACCESS_TOKEN_EXPIRE_MINUTES = AuthConfig().token_expires_minutes

router = APIRouter()


@router.post("/token", response_model=Token)
async def login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    session=Depends(get_session)
):
    user = UserService(session).authenticate_user(
        form_data.username, form_data.password
    )
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"}
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username},
        expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}
