from typing import List, Optional
from regex import R
from sqlmodel import Session
from fastapi import APIRouter, Depends, Query, UploadFile, File
from fastapi.responses import FileResponse

from ..models.user_models import User, UserCreate, UserRead, UserUpdate
from ..models.profile_models import Profile, ProfileRead, ProfileUpdate
from ..models.group_models import Group
from ..models.relationship_models import (UserReadWithProfile,
                                          UserReadWithDex,
                                          GroupReadWithProfiles)
from ..service.user_service import UserService
from ..service.profile_service import ProfileService
from ..service.group_service import GroupService
from ..setup.api_dependencies import get_session, get_current_user

router = APIRouter(
    prefix="/me"
)

@router.patch("/capture",
             summary="Add a profile to your own dex")
def capture_profile(*,
                    session: Session = Depends(get_session),
                    user: User = Depends(get_current_user),
                    profile_id: str):
    user_id = user.id
    db_user = UserService(session).add_profile_to_dex(user_id, profile_id)
    user = UserRead.from_orm(db_user)
    profile = ProfileRead.from_orm(db_user.dex[-1])
    return {
        "user": user,
        "added_profile": profile
    }

@router.patch("/user/update", 
              response_model=UserRead,
              summary="Update your own user info")
def update_user(*,
                session: Session = Depends(get_session),
                user: User = Depends(get_current_user),
                new_user: UserUpdate):
    return UserService(session).update_user(user.id, new_user)


@router.get("/user/with_profile", 
            response_model=UserReadWithProfile, 
            summary="Get your own information, profile included")
def read_me_with_profile(*,
                         session: Session = Depends(get_session),
                         user: User = Depends(get_current_user)):
    return user

@router.get("/user/with_dex",
            response_model=UserReadWithDex, 
            summary="Get your own information, dex included")
def read_me_with_dex(*,
                     session: Session = Depends(get_session),
                     user: User = Depends(get_current_user)):
    return UserService(session).read_user(user.id)


@router.get('/profile/read',
            response_model=ProfileRead,
            summary="Read your own profile")
def read_my_profile(*,
                    session: Session = Depends(get_session),
                    user: User = Depends(get_current_user)):
    profile_id = user.profile_id
    return ProfileService(session).read_profile(profile_id)


#@router.patch("/profile/update",
#              response_model=ProfileRead,
#              summary="Update your own profile")
#def update_my_profile(*,
#                      session: Session = Depends(get_session),
#                      user: User = Depends(get_current_user),
#                      profile: ProfileUpdate = Depends(),
#                      file: Optional[UploadFile] = File(None)):
#    profile_id = user.profile_id
#    return ProfileService(session).update_profile(profile_id, profile, file)

@router.patch("/profile/update",
              response_model=ProfileRead,
              summary="Update your own profile")
def update_my_profile(*,
                      session: Session = Depends(get_session),
                      user: User = Depends(get_current_user),
                      new_profile: ProfileUpdate):
    profile_id = user.profile_id
    return ProfileService(session).update_profile(profile_id, new_profile)

@router.patch("/profile/picture",
              response_model=ProfileRead,
              summary="Update your profile picture")
def update_my_profile_picture(*,
                              session: Session = Depends(get_session),
                              user: User = Depends(get_current_user),
                              file: UploadFile = File(None)):
    profile_id = user.profile_id
    return ProfileService(session).update_picture(profile_id, file)


@router.get("/group/read",
            response_model=GroupReadWithProfiles,
            summary="Read yout own group")
def read_group(*,
               session: Session = Depends(get_session),
               user: User = Depends(get_current_user)):
    group_id = user.profile.group_id
    return GroupService(session).read_group(group_id)

@router.get("/qr_code",
         response_class=FileResponse,
         summary="Get your QR Code")
def read_qr_code(*,
                 session: Session = Depends(get_session),
                 user: User = Depends(get_current_user)):
    profile = ProfileService(session).read_profile(user.profile_id)
    code_name = f"{profile.qr_code}.jpg"
    return f"qr_codes/{code_name}"

